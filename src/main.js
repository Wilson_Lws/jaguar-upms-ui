import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import { loadComponents } from '@/components'
import globalError from './globalError'

window.vue = createApp(App)
window.vue.use(store).use(router).mount('#app')

loadComponents(window.vue)
globalError(window.vue)
