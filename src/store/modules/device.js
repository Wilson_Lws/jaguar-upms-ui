import storage from '@/utils/storage'

const state = () => ({
  device: {},
  deviceUser: storage.get('deviceUser'),
})

const mutations = {
  SET_DEVICE(state, device) {
    state.device = device
  },
  SET_DEVICE_USER(state, deviceUser) {
    storage.set('deviceUser', deviceUser)
    state.deviceUser = deviceUser
  },
  CLEAR_DEVICE_USER(state) {
    storage.remove('deviceUser')
    state.deviceUser = null
  },
}

const actions = {}

export default {
  namespaced: true,
  state: state,
  actions: actions,
  mutations: mutations,
}
