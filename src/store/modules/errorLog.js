import storage from '@/utils/storage'

const state = {
  logs: storage.get('errorLogs') || [],
}

const mutations = {
  ADD_ERROR_LOG: (state, log) => {
    state.logs.push(log)
    storage.set('errorLogs', state.logs)
  },
  CLEAR_ERROR_LOG: (state) => {
    state.logs.splice(0)
    storage.remove('errorLogs')
  },
}

const actions = {
  addErrorLog({ commit }, log) {
    commit('ADD_ERROR_LOG', log)
  },
  clearErrorLog({ commit }) {
    commit('CLEAR_ERROR_LOG')
  },
}

export default {
  namespaced: true,
  state,
  mutations,
  actions,
}
