import router, { loadDynamicRoutes } from '@/router'
import { getMenus } from '@/api/loginUser'
import storage from '@/utils/storage'

const state = () => ({
  menus: storage.get('menus') || [],
  indexMenu: storage.get('indexMenu'),
  permissions: storage.get('permissions') || [],
})

const mutations = {
  SET_MENUS(state, menus = []) {
    storage.set('menus', menus)
    state.menus = menus
  },
  CLEAR_MENUS(state) {
    storage.remove('menus')
    state.menus = []
  },
  SET_INDEX_MENU(state, menu) {
    storage.set('indexMenu', menu)
    state.indexMenu = menu
  },
  CLEAR_INDEX_MENU(state) {
    storage.remove('indexMenu')
    state.indexMenu = null
  },
  SET_PERMISSIONS(state, permissions = []) {
    storage.set('permissions', permissions)
    state.permissions = permissions
  },
  CLEAR_PERMISSIONS(state) {
    storage.remove('permissions')
    state.permissions = []
  },
  UPDATE_MENU_BADGE(state, { menu, badge }) {
    menu.badge = badge
  },
}

const actions = {
  getMenus({ commit, state }) {
    getMenus().then((res) => {
      let menus = res.data.data

      let permissions = []
      for (let i in menus) {
        let menu = menus[i]
        if (menu.menuPermission) permissions.push(menu.menuPermission)
        if (menu.index) {
          if (state.indexMenu) {
            menu.index = false
          } else {
            commit('SET_INDEX_MENU', menu)
          }
        }

        for (let j in menu.children) {
          let child = menu.children[j]
          if (child.menuPermission) permissions.push(child.menuPermission)
          if (child.index) {
            if (state.indexMenu) {
              child.index = false
            } else {
              commit('SET_INDEX_MENU', child)
            }
          }

          for (let k in child.children) {
            permissions.push(child.children[k].menuPermission)
          }
        }
      }

      //没有首页，将第一个菜单设置为首页
      if (!state.indexMenu) {
        outer: for (let i in menus) {
          let menu = menus[i]
          if (menu.menuType == 'MENU') {
            menu.index = true
            commit('SET_INDEX_MENU', menu)
            break
          } else if (menu.menuType == 'CATALOG') {
            for (let j in menu.children) {
              let child = menu.children[j]
              if (child.menuType == 'MENU') {
                child.index = true
                commit('SET_INDEX_MENU', child)
                break outer
              }
            }
          }
        }
      }

      commit('SET_MENUS', menus)
      commit('SET_PERMISSIONS', permissions)
      loadDynamicRoutes(menus)

      const redirectUrl = storage.get('redirectUrl')
      if (redirectUrl) {
        storage.remove('redirectUrl')
        window.location.href = redirectUrl
      } else {
        router.push('/index')
      }
    })
  },
}

export default {
  namespaced: true,
  state: state,
  actions: actions,
  mutations: mutations,
}
