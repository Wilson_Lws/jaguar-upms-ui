import { createStore, createLogger } from 'vuex'
import getters from './getters'

const modules = {}
const modulesFiles = import.meta.glob('./modules/*.js', { eager: true })
Object.keys(modulesFiles).forEach((filePath) => {
  const fileName = filePath.split('/')[2]
  const moduleName = fileName.substr(0, fileName.length - 3)
  modules[moduleName] = modulesFiles[filePath].default
  modules[moduleName]['namespaced'] = true
})

const debug = import.meta.env.NODE_ENV !== 'production'

export const store = createStore({
  modules,
  getters,
  strict: debug,
  plugins: debug ? [createLogger()] : [],
})

export default store
