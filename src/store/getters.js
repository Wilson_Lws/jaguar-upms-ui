export default {
  dark: (state) => state.theme.dark,

  size: (state) => state.theme.size,

  color: (state) => state.theme.color,

  menuBgTransparent: (state) => state.theme.menuBgTransparent,

  collapse: (state) => state.theme.collapse,

  keepAlive: (state) => state.theme.keepAlive,

  // 用户token
  loginUserToken: (state) => state.loginUser.token,
  // 用户信息
  loginUserInfo: (state) => state.loginUser.info,
  // 授权部门
  depts: (state) => state.loginUser.depts,

  // 菜单列表
  menus: (state) => state.menu.menus,
  // 权限列表
  permissions: (state) => state.menu.permissions,
  // 首页菜单
  indexMenu: (state) => state.menu.indexMenu,

  errorLogs: (state) => state.errorLog.logs,

  device: (state) => state.device.device,

  deviceUser: (state) => state.device.deviceUser,
}
