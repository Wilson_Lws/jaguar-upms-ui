import request from '@/api/axios'

const baseurl = '/captcha'

//获取验证图片  以及token
export function reqGet(data) {
  return request({
    url: baseurl + '/get',
    method: 'post',
    data,
  })
}

//滑动或者点选验证
export function reqCheck(data) {
  return request({
    url: baseurl + '/check',
    method: 'post',
    data,
  })
}
