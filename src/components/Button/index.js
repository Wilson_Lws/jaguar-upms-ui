import AddButton from './AddButton'
import CancelButton from './CancelButton'
import DeleteButton from './DeleteButton'
import EditButton from './EditButton'
import RefreshButton from './RefreshButton'
import ResetButton from './ResetButton'
import SaveButton from './SaveButton'
import SearchButton from './SearchButton'
import ViewButton from './ViewButton'

export default {
  AddButton,
  CancelButton,
  DeleteButton,
  EditButton,
  RefreshButton,
  ResetButton,
  SaveButton,
  SearchButton,
  ViewButton,
}
