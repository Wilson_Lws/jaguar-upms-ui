import store from '@/store'

import 'normalize.css'
import 'nprogress/nprogress.css'

import { ElMessage, ElMessageBox } from 'element-plus'
import zhCn from 'element-plus/es/locale/lang/zh-cn'
import 'element-plus/dist/index.css'
import 'element-plus/theme-chalk/dark/css-vars.css'

import '@/assets/style/element.scss'
import '@/assets/style/jaguar.scss'

import Buttons from './Button'
import SearchInput from './SearchForm/SearchInput'
import SearchSelect from './SearchForm/SearchSelect'
import DictionaryLabel from './DictionaryLabel'
import TablePage from './TablePage'
import UserSelect from './UserSelect'
import DeptTreeSelect from './DeptTreeSelect'
import JInputWrapper from './JInputWrapper'

import 'virtual:svg-icons-register'
import IconSvg from './IconSvg.vue'
import * as ElementPlusIconsVue from '@element-plus/icons-vue'

import ThemeSwitcher from './ThemeSwitcher/index.vue'

import mitt from 'mitt'

// 加载全局组件
export function loadComponents(app) {
  app.use(ElMessage, { zhCn })
  app.use(ElMessageBox, { zhCn })

  app.component('icon-svg', IconSvg)
  for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
  }

  for (let key in Buttons) {
    app.component(key, Buttons[key])
  }

  app.component('search-input', SearchInput)
  app.component('search-select', SearchSelect)
  app.component('dictionary-label', DictionaryLabel)
  app.component('table-page', TablePage)
  app.component('dept-tree-select', DeptTreeSelect)
  app.component('user-select', UserSelect)
  app.component('j-input-wrapper', JInputWrapper)
  app.directive('hasPermission', {
    mounted(el, binding) {
      for (let i in store.getters.permissions) {
        if (binding.value == store.getters.permissions[i]) {
          return
        }
      }
      el.parentNode.removeChild(el)
    },
  })

  app.component('theme-switcher', ThemeSwitcher)

  app.config.globalProperties.$bus = new mitt()
}
