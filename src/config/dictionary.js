export const yesOrNo = [
  { label: '是', value: true },
  { label: '否', value: false },
]

export const isEnable = [
  { label: '启用', value: true },
  { label: '禁用', value: false },
]

export const menuType = [
  { label: '目录', value: 'CATALOG' },
  { label: '菜单', value: 'MENU' },
  { label: '权限', value: 'AUTHORITY' },
]

export default {
  yesOrNo,
  isEnable,
  menuType,
}
