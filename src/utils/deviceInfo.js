import packageInfo from '@/../package.json'
import Fingerprint2 from 'fingerprintjs2'

function getBrowserInfo() {
  let agent = navigator.userAgent.toLowerCase()

  let regStr_ie = /msie [\d.]+;/gi
  let regStr_ff = /firefox\/[\d.]+/gi
  let regStr_chrome = /chrome\/[\d.]+/gi
  let regStr_saf = /safari\/[\d.]+/gi
  //IE
  if (agent.indexOf('msie') > 0) {
    return agent.match(regStr_ie)
  }

  //firefox
  if (agent.indexOf('firefox') > 0) {
    return agent.match(regStr_ff)
  }

  //Chrome
  if (agent.indexOf('chrome') > 0) {
    return agent.match(regStr_chrome)
  }

  //Safari
  if (agent.indexOf('safari') > 0 && agent.indexOf('chrome') < 0) {
    return agent.match(regStr_saf)
  }
}

function getOsInfo() {
  var userAgent = navigator.userAgent.toLowerCase()
  var name = 'Unknown'
  var version = 'Unknown'
  if (userAgent.indexOf('win') > -1) {
    name = 'Windows'
    if (userAgent.indexOf('windows nt 5.0') > -1) {
      version = '2000'
    } else if (
      userAgent.indexOf('windows nt 5.1') > -1 ||
      userAgent.indexOf('windows nt 5.2') > -1
    ) {
      version = 'XP'
    } else if (userAgent.indexOf('windows nt 6.0') > -1) {
      version = 'Vista'
    } else if (
      userAgent.indexOf('windows nt 6.1') > -1 ||
      userAgent.indexOf('windows 7') > -1
    ) {
      version = '7'
    } else if (
      userAgent.indexOf('windows nt 6.2') > -1 ||
      userAgent.indexOf('windows 8') > -1
    ) {
      version = '8'
    } else if (userAgent.indexOf('windows nt 6.3') > -1) {
      version = '8.1'
    } else if (
      userAgent.indexOf('windows nt 6.2') > -1 ||
      userAgent.indexOf('windows nt 10.0') > -1
    ) {
      version = '10'
    } else {
      version = 'Unknown'
    }
  } else if (userAgent.indexOf('iphone') > -1) {
    name = 'Iphone'
  } else if (userAgent.indexOf('android') > -1) {
    name = 'Android'
  } else if (userAgent.indexOf('mac') > -1) {
    name = 'Mac'
  } else if (userAgent.indexOf('linux') > -1) {
    name = 'Linux'

    let start = userAgent.indexOf('linux') + 6
    let end = userAgent.indexOf(')')
    version = navigator.userAgent.substring(start, end)
  } else if (
    userAgent.indexOf('x11') > -1 ||
    userAgent.indexOf('unix') > -1 ||
    userAgent.indexOf('sunname') > -1 ||
    userAgent.indexOf('bsd') > -1
  ) {
    name = 'Unix'
  } else {
    name = 'Unknown'
  }
  return { name, version }
}

function getFingerprint() {
  return new Promise((resolve) => {
    Fingerprint2.get((components) => {
      var values = components.map((component, index) => {
        if (index === 0) {
          //把微信浏览器里UA的wifi或4G等网络替换成空,不然切换网络会ID不一样
          return component.value.replace(/\bNetType\/\w+\b/, '')
        }

        return component.value
      })

      resolve(Fingerprint2.x64hash128(values.join(''), 31))
    })
  })
}

export async function loadDeviceInfo() {
  let browserInfo = getBrowserInfo()[0]
  let osInfo = getOsInfo()
  let deviceUid = await getFingerprint()

  let deviceInfo = {
    deviceUid: deviceUid,
    deviceName: browserInfo,
    deviceType: 'PC',
    deviceModel: osInfo.name,
    deviceSysVersion: osInfo.version,
    deviceClientVersion: packageInfo.version,
    deviceLocalAuth: false,
  }

  return deviceInfo
}
