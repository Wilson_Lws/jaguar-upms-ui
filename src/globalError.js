import { nextTick } from 'vue'
import store from '@/store'

export default (vue) => {
  vue.config.errorHandler = function (err, vm, info) {
    nextTick(() => {
      store.dispatch('errorLog/addErrorLog', {
        err: {
          message: err.toString(),
          stack: err.stack ? err.stack.toString() : '',
        },
        vmName: vm.$options.name,
        info,
        url: window.location.href,
        timestamp: Date.now(),
      })
      console.error(err, info)
    })
  }
}
