import qs from 'qs'
import { ElMessage as Message, ElLoading as Loading } from 'element-plus'

import axios from 'axios'
import store from '@/store'

// 跨域请求，允许保存cookie
axios.defaults.withCredentials = false
// create an axios instance
const service = axios.create({
  baseURL: window.baseURL.upms,
  // withCredentials: true, // send cookies when cross-domain requests
  timeout: 15000, // request timeout
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Content-Type': 'application/json; charset=UTF-8',
  },
  paramsSerializer: (params) => {
    return qs.stringify(params, { arrayFormat: 'repeat' })
  },
})

let loadingInstance

// request interceptor
service.interceptors.request.use(
  (config) => {
    // do something before request is sent
    let meta = Object.assign({ showLoading: false }, config.meta)
    // showLoading 为true时 显示loading
    if (meta.showLoading) {
      loadingInstance = Loading.service({
        lock: true,
        text: '加载中...',
        spinner: 'loading',
        background: 'rgba(0, 0, 0, 0.3)',
      })
    }

    if (config.method === 'get') {
      config.params = Object.assign(config.params || {}, {
        timeVersion: Date.now(),
      })
    }

    if (store.getters.loginUserToken) {
      config.headers['Authorization'] = store.getters.loginUserToken
    }
    return config
  },
  (error) => {
    // do something with request error
    console.error(error) // for debug
    if (loadingInstance) loadingInstance.close()
    return Promise.reject(error)
  }
)

// response interceptor
service.interceptors.response.use(
  /**
   * If you want to get http information such as headers or status
   * Please return  res => res
   */

  /**
   * Determine the request status by custom code
   * Here is just an example
   * You can also judge the status by HTTP Status Code
   */
  (res) => {
    if (loadingInstance) loadingInstance.close()
    const status = res.status
    const data = res.data

    if (status !== 200) {
      Message({
        message: '网络错误，状态码：' + status,
        type: 'error',
      })
      return Promise.reject(res)
    }

    if (data.resultCode == 200) {
      return res
    }

    let message = data.message
    switch (data.resultCode) {
      case 302: {
        window.location.href = data.data
        if (message) {
          Message({
            message,
            type: 'success',
          })
        }
        return res
      }
      case 401: {
        if (store.getters.loginUserToken) {
          store.dispatch('loginUser/gotoLogin')
        } else {
          message = null
        }
        break
      }
      case 400: {
        message = message || '请求错误'
        break
      }
      case 404: {
        message = message || '没有找到接口'
        break
      }
      case 409: {
        message = message || '业务冲突'
        break
      }
      case 500: {
        message = message || '服务器内部错误'
        break
      }
      case 502: {
        message =
          '微服务【' +
          data.data.server +
          '】接口【' +
          data.data.method +
          '】调用失败'
        break
      }
      case 503: {
        message = '微服务不可用：' + message
        break
      }
      default: {
        message = message || '未知错误'
      }
    }

    if (message) {
      Message({
        message: message,
        type: 'error',
      })
    }

    return Promise.reject(res)
  },
  (error) => {
    if (loadingInstance) loadingInstance.close()

    let status = error.response ? error.response.status : -1
    let message
    if (status == -1) {
      message = '服务器无响应'
    } else if (status == 404) {
      message = '没有找到接口'
    } else if (status == 502 || status == 503) {
      message = '系统升级中，请稍后再试！'
    } else if (status == 504) {
      message = '请求超时！'
    } else {
      message = '请求失败！'
    }

    Message({
      message,
      type: 'error',
    })
    return Promise.reject(error)
  }
)

export default service
