import request from '@/api/axios'

const baseurl = ''

export function login(params) {
  return request({
    url: baseurl + '/login',
    method: 'post',
    params,
  })
}

export function logout() {
  return request({
    url: baseurl + '/logout',
    method: 'post',
  })
}

export function faceLogin(params, faceImage) {
  return request({
    url: baseurl + '/login/face',
    method: 'post',
    params,
    data: faceImage,
    headers: {
      'Content-Type': 'text/plain',
    },
  })
}

export function deviceLogin(params) {
  return request({
    url: baseurl + '/login/device',
    method: 'post',
    params,
  })
}

export function smsLogin(params) {
  return request({
    url: baseurl + '/login/sms',
    method: 'post',
    params,
  })
}

export function sendEmailSms(params) {
  return request({
    url: baseurl + '/sms/email',
    method: 'post',
    params,
  })
}

export function loginUserInfo() {
  return request({
    url: baseurl + '/loginUser/info',
    method: 'get',
  })
}
