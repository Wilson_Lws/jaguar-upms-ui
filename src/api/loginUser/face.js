import request from '@/api/axios'

export function face(data) {
  return request({
    url: '/face',
    method: 'PUT',
    data,
    headers: {
      'Content-Type': 'text/plain',
    },
  })
}
