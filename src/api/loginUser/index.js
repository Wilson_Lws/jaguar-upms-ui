import request from '@/api/axios'

const baseurl = '/loginUser'

export function getInfo() {
  return request({
    url: baseurl + '/info',
    method: 'get',
  })
}

export function putInfo(params) {
  return request({
    url: baseurl + '/info',
    method: 'put',
    params,
  })
}

export function putPassword(params) {
  return request({
    url: baseurl + '/password',
    method: 'put',
    params,
  })
}

export function getMenus(params) {
  return request({
    url: baseurl + '/menus',
    method: 'get',
    params,
  })
}

export function fetchRoleList() {
  return request({
    url: baseurl + '/role/list',
    method: 'get',
  })
}
