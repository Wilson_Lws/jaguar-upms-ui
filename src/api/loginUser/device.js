import request from '@/api/axios'

const baseurl = '/loginUser/device'

export function fetchList(params) {
  return request({
    url: baseurl + '/list',
    method: 'get',
    params,
  })
}

export function bindDevice(data, params) {
  return request({
    url: baseurl + '/bindDevice',
    method: 'post',
    params,
    data,
  })
}

export function unbindDevice(deviceUid) {
  return request({
    url: baseurl + '/unbindDevice/' + deviceUid,
    method: 'delete',
  })
}
