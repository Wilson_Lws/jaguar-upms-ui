import request from '@/api/axios'

const baseurl = '/common'

export function randomCode(params) {
  return request({
    url: baseurl + '/randomCode',
    method: 'get',
    params,
  })
}

export function fetchUserPage(params) {
  return request({
    url: baseurl + '/user/page',
    method: 'get',
    params,
  })
}

export function fetchDeptTree(parentId) {
  return request({
    url: baseurl + '/dept/tree',
    method: 'get',
    params: {
      parentId,
    },
  })
}
