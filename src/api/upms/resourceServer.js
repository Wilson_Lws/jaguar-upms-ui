import request from '@/api/axios'

const baseurl = '/resourceServer'

export function fetchList(query) {
  for (const key in query) {
    if (query[key] == null || query[key] === '') {
      delete query[key]
    }
  }
  return request({
    url: baseurl + '/list',
    method: 'get',
    params: query,
  })
}

export function del(serverId) {
  return request({
    url: baseurl + '/' + serverId,
    method: 'delete',
  })
}
