import request from '@/api/axios'

const baseurl = '/menu'

export function fetchTree() {
  return request({
    url: baseurl + '/tree',
    method: 'get',
  })
}

export function getDetail(id) {
  return request({
    url: baseurl + '/' + id,
    method: 'get',
  })
}

export function save(data) {
  return request({
    url: baseurl,
    method: 'post',
    data,
  })
}

export function update(data) {
  return request({
    url: baseurl,
    method: 'put',
    data,
  })
}

export function del(id) {
  return request({
    url: baseurl + '/' + id,
    method: 'delete',
  })
}
