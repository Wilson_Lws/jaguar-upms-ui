import request from '@/api/axios'

const baseurl = '/dept'

export function fetchList(query) {
  for (const key in query) {
    if (query[key] == null || query[key] === '') {
      delete query[key]
    }
  }
  return request({
    url: baseurl + '/tree',
    method: 'get',
    params: query,
  })
}

export function getDetail(id) {
  return request({
    url: baseurl + '/' + id,
    method: 'get',
  })
}

export function save(data) {
  return request({
    url: baseurl,
    method: 'post',
    data,
  })
}

export function update(data) {
  return request({
    url: baseurl,
    method: 'put',
    data,
  })
}

export function del(id) {
  return request({
    url: baseurl + '/' + id,
    method: 'delete',
  })
}
