import request from '@/api/axios'

const baseurl = window.origin + window.baseURL.handlerLog + '/handlerLog'

export function fetchList(query) {
  for (const key in query) {
    if (query[key] == null || query[key] === '') {
      delete query[key]
    }
  }
  return request({
    url: baseurl + '/list',
    method: 'get',
    params: query,
  })
}

export function fetchApiOperation() {
  return request({
    url: baseurl + '/listApiOperation',
    method: 'get',
  })
}

export function getDetail(id) {
  return request({
    url: baseurl + '/' + id,
    method: 'get',
  })
}
