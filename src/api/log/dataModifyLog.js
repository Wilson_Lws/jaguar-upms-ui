import request from '@/api/axios'

const baseurl = '/dataModifyLog'

export function fetchList(query) {
  for (const key in query) {
    if (query[key] == null || query[key] === '') {
      delete query[key]
    }
  }
  return request({
    url: baseurl + '/page',
    method: 'get',
    params: query,
  })
}

export function getDetail(id) {
  return request({
    url: baseurl + '/' + id,
    method: 'get',
  })
}
