import qs from 'qs'
import request from '@/api/axios'

const baseurl = '/oauth2'

export function authorize(params) {
  return request({
    url: baseurl + '/authorize',
    method: 'get',
    params,
  })
}

export function confirmAccess(params) {
  return request({
    url: baseurl + '/authorize',
    method: 'post',
    params,
    paramsSerializer: (params) => {
      return qs.stringify(params, { arrayFormat: 'repeat' })
    },
  })
}

export function token(params) {
  return request({
    url: baseurl + '/token',
    method: 'post',
    params,
  })
}

export function revoke(params) {
  return request({
    url: baseurl + '/revoke',
    method: 'post',
    params,
    meta: {
      showLoading: true,
    },
  })
}
