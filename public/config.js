window.baseURL = {
  upms: '/api/jaguar-upms-server',
  handlerLog: '/api/jaguar-handler-log-server',
  fileStorage: '/api/jaguar-file-storage-server',
}

window.clientName = '用户权限管理系统'

window.config = {
  faceEnabled: false,
  smsEnabled: false,
  verifyCodeEnabled: true,
}

document.getElementsByTagName('title')[0].innerText = window.clientName
