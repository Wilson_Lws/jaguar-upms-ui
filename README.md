# 用户权限管理系统页面

## 开发环境

- Win10 或 Win11
- [node@20:15.0](https://nodejs.org/en/blog/release/v20.15.0/)
- npm@9.9.2
- cnpm@9.4.0

## 开发环境搭建

1. 使用下载好的安装包安装 node 和 npm

2. 使用命令行安装 cnpm

```shell
  npm install -g cnpm@9.4.0 --registry=https://registry.npmmirror.com
```

3. 使用命令行下载依赖库，并且启动服务

```shell
  cnpm install
  cnpm run dev
```

## 传统打包部署

1. 使用命令行打包生成 dist 文件夹

```shell
  cnpm install
  cnpm run build
```

2. 如果使用 springboot 直接代理，则将 dist 下的所有文件拷贝到与 app.jar 同级目录的 static 文件夹下

3. 如果使用 nginx 代理，则需要根据 nginx 配置文件，将 dist 下的所有文件拷贝到指定文件夹下

4. 根据部署方式修改 [config.js](public/config.js) 配置文件中的 `window.baseURL` 参数值

## 使用 docker 打包部署

1. 使用命令行构建 docker 镜像并推送到阿里云仓库

```shell
  cnpm run docker:build
  cnpm run docker:push
```

2. 使用后台的 docker-compose.yml 启动
