FROM  alpine

WORKDIR /home

ADD dist dist

ENTRYPOINT rm -rf static/* && mv -f dist/* static/